// SPDX-FileCopyrightText: (C) The MADness project
// SPDX-License-Identifier: MIT-0
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include <ctss.h>

int64_t chfile (int64_t name1, int64_t name2, int64_t mode, int64_t old1)
{
   int stat;
   char otmp[64];
   char ntmp[64];

   GetFileStr (otmp, name1, name2);
   GetFileStr (ntmp, old1, name2);
#ifdef DEBUG
   fprintf (stderr, "chfile:, otmp = %s, ntmp = %s\n", otmp, ntmp);
#endif
   rename (otmp, ntmp);
}
