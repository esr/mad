// SPDX-FileCopyrightText: (C) The MADness project
// SPDX-License-Identifier: MIT-0
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <ctss.h>

extern int CtssArgc;
extern char **CtssArgv;

int64_t comarg (int64_t arg)
{
   if (arg > (CtssArgc - 1))
      return FENCE;
   return (PackStr(CtssArgv[arg]));
}
