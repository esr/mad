// SPDX-FileCopyrightText: (C) The MADness project
// SPDX-License-Identifier: MIT-0
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#include <ctss.h>

FILE *TapeOpen(FILE *tfd, char *mode, int64_t lun)
{
   FILE *fd = NULL;
   char filename[32];

   if (tfd != NULL) {
      return tfd;
   }
   sprintf(filename, "Tape_%ld.dat", lun);
   if ((fd = fopen(filename, mode)) == NULL) {
      fprintf(stderr, "Can't open TAPE file: %s(%s): %s\n",
               filename, mode, strerror(errno));
      exit(1);
   }
   return fd;
}

FILE *TapeClose(FILE *tfd, int64_t lun, int unload)
{
   if (tfd == NULL) {
      return NULL;
   }
   if (fclose(tfd) < 0) {
      fprintf(stderr, "Can't %s TAPE: Tape_%ld: %s\n",
               unload ? "unload" : "write EOF", lun, strerror(errno));
      exit(1);
   }
   return NULL;
}

void TapeRewind(FILE *tfd, int64_t lun, long *loc)
{
   if (tfd == NULL) {
      return;
   }
   if (fseek(tfd, 0, SEEK_SET) < 0) {
      fprintf(stderr, "Can't rewind TAPE: Tape_%ld: %s\n",
               lun, strerror(errno));
      exit(1);
   }
   *loc = 0;
}

void TapeBackspace(FILE *tfd, int64_t lun, long *loc)
{
   if (fseek(tfd, *loc, SEEK_SET) < 0) {
      fprintf(stderr, "Can't BACKSPACE TAPE: Tape_%ld: %s\n",
               lun, strerror(errno));
      exit(1);
   }
}

void TapeReadBinary(TapeVector *vecs, FILE *tfd, int64_t lun, int cnt,
                     long *loc, int64_t *eof)
{
   int i;

   *loc = ftell(tfd);
   for (i = 0; i < cnt; i++) {
      if (fread(vecs[i].ptr, 1, vecs[i].len, tfd) != vecs[i].len) {
         if (feof(tfd)) {
            if (eof != NULL) {
               *eof = 1LL;
               return;
            }
         } else {
            fprintf(stderr, "Can't READ BINARY TAPE: Tape_%ld: %s\n",
                     lun, strerror(ferror(tfd)));
         }
         exit(1);
      }
   }
}

void TapeWriteBinary(TapeVector *vecs, FILE *tfd, int64_t lun, int cnt,
                      long *loc)
{
   int i;

   *loc = ftell(tfd);
   for (i = 0; i < cnt; i++) {
      if (fwrite(vecs[i].ptr, 1, vecs[i].len, tfd) != vecs[i].len) {
         if (!feof(tfd)) 
            fprintf(stderr, "Can't WRITE BINARY TAPE: Tape_%ld: %s\n",
                     lun, strerror(ferror(tfd)));
         exit(1);
      }
   }
}

void seteof(int64_t *lbl)
{
   *lbl = 0LL;
}


