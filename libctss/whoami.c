// SPDX-FileCopyrightText: (C) The MADness project
// SPDX-License-Identifier: MIT-0
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>

#include <ctss.h>

void whoami (int64_t *arg, int64_t cnt)
{
   int i;
   char tmp[32];

   if (cnt > 6) cnt = 6;
   for (i = 0; i < cnt; i++) {
      gid_t gid;
      uid_t uid;
      switch (i) {
         case 0: /* PROBNO, gid */
	    gid = getgid();
	    sprintf (tmp, "%06d", (int)gid);
	    arg[i] = PackStr(tmp);
	    break;
         case 1: /* PROGNO, uid */
	    uid = getuid();
	    sprintf (tmp, "%06d", (int)uid);
	    arg[i] = PackStr(tmp);
	    break;
         case 2: /* SYSNAM, CTSS */
	    arg[i] = PackStr("CTSS  ");
	    break;
         default:
	    arg[i] = PackStr("DEFALT");
	    break;
      }
   }
}
