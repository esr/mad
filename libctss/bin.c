// SPDX-FileCopyrightText: (C) The MADness project
// SPDX-License-Identifier: MIT-0
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <ctss.h>

int64_t bin (int64_t arg)
{
   int64_t x, r;
   int i;

   r = 0;
   for (i = 0; i < 6; i++) {
      x = (arg & 0xFF0000000000L) >> 40;
      if (isdigit(x))
         r = (r * 10) + (x - '0');
      else
         r = r * 10;
      arg <<= 8;
   }
   return r;
}
