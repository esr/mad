// SPDX-FileCopyrightText: (C) The MADness project
// SPDX-License-Identifier: MIT-0
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include <ctss.h>

int64_t Crename (int64_t old1, int64_t old2, int64_t new1, int64_t new2)
{
   int stat;
   char ntmp[64];
   char otmp[64];

   GetFileStr (otmp, old1, old2);
   GetFileStr (ntmp, new1, new2);
#ifdef DEBUG
   fprintf (stderr, "rename:, otmp = %s, ntmp = %s\n", otmp, ntmp);
#endif
   stat = rename (otmp, ntmp);
}
