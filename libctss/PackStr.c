// SPDX-FileCopyrightText: (C) The MADness project
// SPDX-License-Identifier: MIT-0
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>

#include <ctss.h>

int64_t PackStr (const char *s)
{
   int64_t r;
   int ch;

   r = 0x202020202020L;
   for (;*s;s++) {
      ch = *s;
      if (islower(ch)) ch = toupper(ch);
      r = (r << 8) | ch;
   }
   return r & 0xFFFFFFFFFFFFL;
}

void UnpackStr (char *str, int64_t arg)
{
   int64_t x;
   int i;

   for (i = 0; i < 6; i++) {
      x = (arg & 0xFF0000000000LL) >> 40;
      if (!isspace(x))
         *str++ = x;
      arg <<= 8;
   }
   *str = 0;
}

void GetFileStr (char *name, int64_t arg1, int64_t arg2)
{
   char *cp;
   char tmp[32];

   UnpackStr (name, arg1);
   UnpackStr (tmp, arg2);
   strcat (name, ".");
   strcat (name, tmp);
   for (cp = name; *cp; cp++) {
      if (isupper(*cp))
         *cp = tolower(*cp);
   }
}
