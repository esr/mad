/************************************************************************
* MAD CTSS header.
************************************************************************/
// SPDX-FileCopyrightText: (C) The MADness project
// SPDX-License-Identifier: MIT-0

#ifndef __CTSS_H__
#define __CTSS_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#ifndef True
#define True 1
#endif
#ifndef False
#define False 0
#endif

#define MAXCTSSFILE 10
#define FENCE 0777777777777LL

typedef int64_t bool;

typedef struct {
   FILE *fd;
   int  mode;
   int  inuse;
   char name[64];
} CtssFile_t;

typedef struct {
   char *var;
   int64_t *loc;
} FmtVar;

typedef struct {
   void *ptr;
   int len;
} TapeVector;

typedef union {
   int64_t intv;
   bool boolv;
   double dblv;
} TapeVars;

extern char ErrFmt[256];

extern int64_t PackStr (const char *);
extern void UnpackStr (char *, int64_t);
extern void GetFileStr (char *, int64_t, int64_t);

extern void ProcessFmt(FmtVar *, char *, char *, int, int);

extern FILE *TapeOpen(FILE *, char *, int64_t);
extern FILE *TapeClose(FILE *, int64_t, int);
extern void TapeRewind(FILE *, int64_t, long *);
extern void TapeBackspace(FILE *, int64_t, long *);
extern void TapeReadBinary(TapeVector *, FILE *, int64_t, int, long *,
			   int64_t *);
extern void TapeWriteBinary(TapeVector *, FILE *, int64_t, int, long *);
extern void seteof(int64_t *);

extern int64_t assign (int64_t, int64_t);

extern int64_t bin(int64_t);

extern int64_t chfile (int64_t, int64_t, int64_t, int64_t);
extern int64_t chncom (int64_t);
extern int64_t comarg (int64_t);
extern int64_t Cexit (int64_t);
extern int64_t Crename (int64_t, int64_t, int64_t, int64_t);

extern int64_t delfil (int64_t, int64_t, int64_t *);

extern int64_t seek (int64_t, int64_t, int64_t *);

extern void whoami (int64_t *, int64_t);

#endif
