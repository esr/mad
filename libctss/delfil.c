// SPDX-FileCopyrightText: (C) The MADness project
// SPDX-License-Identifier: MIT-0
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include <ctss.h>

int64_t delfil (int64_t name1, int64_t name2, int64_t *lbl)
{
   int stat;
   char tmp[64];

   GetFileStr (tmp, name1, name2);
   *lbl = 0;
   stat = unlink (tmp);
}
