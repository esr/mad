// SPDX-FileCopyrightText: (C) The MADness project
// SPDX-License-Identifier: MIT-0
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include <ctss.h>

CtssFile_t CtssFiles[MAXCTSSFILE];

static int firstentry = TRUE;

int64_t assign (int64_t name1, int64_t name2)
{
   CtssFile_t *cp;
   FILE *fd;
   int i;
   char name[64];

   if (firstentry) {
      memset (&CtssFiles, 0, sizeof(CtssFiles));
      firstentry = FALSE;
   }

   GetFileStr (name, name1, name2);
   cp = NULL;
   for (i = 0; i < MAXCTSSFILE; i++) {
      if (!CtssFiles[i].inuse) {
	 cp = &CtssFiles[i];
         break;
      } else {
         if (!strcmp (CtssFiles[i].name, name)) {
	    cp = &CtssFiles[i];
	    fclose (cp->fd);
	    break;
	 }
      }
   }
   if (cp) {
      strcpy (cp->name, name);
      cp->inuse = TRUE;
      cp->mode = 'r';

      cp->fd = fopen (cp->name, "r");
   }
}

int64_t seek (int64_t name1, int64_t name2, int64_t *lbl)
{
   FILE *fd;
   CtssFile_t *cp;
   int i;
   char name[64];

   *lbl = 0;
   if (firstentry) {
      memset (&CtssFiles, 0, sizeof(CtssFiles));
      firstentry = FALSE;
   }

   GetFileStr (name, name1, name2);
   cp = NULL;
   for (i = 0; i < MAXCTSSFILE; i++) {
      if (!CtssFiles[i].inuse)
      if (!CtssFiles[i].inuse) {
	 cp = &CtssFiles[i];
         break;
      } else {
         if (!strcmp (CtssFiles[i].name, name)) {
	    cp = &CtssFiles[i];
	    fclose (cp->fd);
	    break;
	 }
      }
   }
   if (cp) {
      strcpy (cp->name, name);
      cp->inuse = TRUE;
      cp->mode = 'r';

      cp->fd = fopen (cp->name, "r");
      if (!cp->fd) *lbl = 1;
   }
}
