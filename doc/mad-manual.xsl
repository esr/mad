<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:param name="make.valid.html" select="1"/>
<xsl:param name="html.cleanup" select="1"/>
<xsl:param name="chunk.first.sections" select="1"/>
<xsl:param name="use.id.as.filename" select="1"/>
<xsl:param name="emphasis.propagates.style" select="1"/>
<xsl:param name="para.propagates.style" select="1"/>
<xsl:param name="phrase.propagates.style" select="1"/>
<xsl:param name="html.stylesheet">mad-manual.css</xsl:param>
<xsl:param name="css.decoration">1</xsl:param>
<xsl:param name="generate.section.toc.level" select="0"></xsl:param>
<xsl:param name="section.autolabel" select="1"></xsl:param>
<xsl:param name="section.label.includes.component.label" select="0"></xsl:param>

</xsl:stylesheet>
