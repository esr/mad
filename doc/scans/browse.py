#!/usr/bin/env python
#
# browae.py - browse the scans
#
"""
browse.py -- browse the manual page scans

Browse the PNG scans of the original manual.  Available commands are:

n - Go to next page(can take integer repeat count).
p - go to previous page (can take integer repeat count).
g - Go to (requires page number or bookmark name).

There are bookmarks for each chapter and for each code example
in the madhouse catalog (by name).
"""

import cmd, readline, subprocess

I = 1
II = 15
III = 64
IV = 121

bookmarks = (
    ("I",          I + 1),
    ("II",         II + 1),
    ("III",        III + 1),
    ("IV",         IV + 1),
    ("A1",         128),
    ("A2",         138),
    ("Supplement", 141),
    ("Index",      147),
    ("end",        154),
    ("maxfunc",    I + 6),
    ("newton1",    I + 9),
    ("newton2",    I + 13),
    ("invsf",      II + 47),
    ("quadratic",  III + 3),
    ("truthtable", III + 6),
    ("simpsfunc",  III + 8),
    ("simpsmain",  III + 9),
    ("findroot",   III + 13),
    ("transfunc",  III + 15),
    ("matrixmult", III + 17),
    ("jordan",     III + 22),
    ("payroll",    III + 2),
    ("altpayroll", III + 28),
    ("payrolltape",III + 35),
    ("mortgage",   III + 40),
    ("commutefunc",III + 45), 
    ("commutemain",III + 45),
    ("wordsearch", III + 49),
    ("factfunc",   III + 50),
    ("factmain",   III + 51),
    ("gcdfunc",    III+ 53),
    ("gcdmain",    III+ 55),
    ("tschebfunc", III+ 57),
    ("tschebmain", III+ 57),
    )

class ScanBrowser(cmd.Cmd):
    "Scan browser class."
    def __init__(self):
        cmd.Cmd.__init__(self)
        self.page = 1
        self.prompt = "> "
        self.proc = None
    def postcmd(self, stop, line):
        if line == "EOF":
            return True
        elif not line.startswith("?") and not line.startswith("l"): 
            if self.proc:
                self.proc.terminate()
            self.proc = subprocess.Popen(("display", "page%03d.png" % self.page))
            print "%d:" % self.page
    def getarg(self, line, base=0, sgn=1):
        line = line.strip()
        if not line:
            return self.page + sgn
        try:
            n = base + int(line)
            if n >= 1 and n < dict(bookmarks)["end"]:
                return n
        except ValueError:
            print "Bar!"
            return None
    def do_n(self, line):
        "Next page (can take integer repeat count)."
        n = self.getarg(line, base=self.page, sgn=1)
        if n:
            self.page = n
        return False
    def do_p(self, line):
        "Previous page (can take integer repeat count)."
        n = self.getarg(line, base=self.page, sgn=-1)
        if n:
            self.page = n
        return False
    def do_l(self, line):
        "List bookmarks."
        print "\n".join(map(lambda x: x[0], bookmarks))
    def do_g(self, line):
        "Go to (requires page number or bookmark name)."
        if line in dict(bookmarks):
            self.page = dict(bookmarks)[line.strip()]
        else:
            n = self.getarg(line)
            if n:
                print "Setting page to", n
                self.page = n
            else:
                print "No such page."
    def do_EOF(self, line):
        "Terminate the browser."
        if self.proc:
            self.proc.terminate()
        return True

if __name__ == '__main__':
    try:
        ScanBrowser().cmdloop()
    except KeyboardInterrupt:
        print ""


