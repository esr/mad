;; Convert ASCII markup to DocBook

(defun madconvert ()
  (interactive)
  (save-excursion
   (query-replace-regexp "_\\(\\w+\\)" "<subscript>\\1</subscript>"))
  (save-excursion
   (query-replace-regexp "\\^\\(\\w+\\)" "<superscript>\\1</superscript>"))
  (save-excursion
   (replace-string "#F" "&scriptF;"))
  (save-excursion
   (replace-string "#L" "&scriptL;"))
  (save-excursion
   (replace-string "#M" "&scriptM;"))
  (save-excursion
   (replace-string "#N" "&scriptN;"))
  (save-excursion
   (replace-string "#S" "&scriptS;"))
  )
