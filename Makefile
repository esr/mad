# mad -- Michigan Algorithmic Decoder
#
# You must install ply with "make p;y" before attempting to run the compike
#
# SPDX-FileCopyrightText: (C) The MADness project
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

CFLAGS = -O
PYTHONPKGS = yacc.py yacc.pyc lex.py lex.pyc

all: ply
	(cd libctss; make)
	(cd madhouse; make MADOPTS=$(MADOPTS) )

ply:
	git clone git@github.com:dabeaz/ply.git

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

clean:
	rm -fr *.rpm *.tar.gz parser.out parsetab.py mad.sed __pycache__ libctss/*.[oa]
	(cd doc; make clean)
	(cd libctss; make clean)
	(cd madhouse; make clean)

install: mad libctss/libctss.a mad.1
	sed -e 's\../libctss\/usr/include\g' <mad >mad.sed
	chmod +x mad.sed
	cp mad.sed $(DESTDIR)/usr/bin/mad
	cp mad.1 $(DESTDIR)/usr/share/man/man1
	cp libctss/ctss.h $(DESTDIR)/usr/include
	cp libctss/libctss.a $(DESTDIR)/usr/lib
	cp $(PYTHONPKGS) $(DESTDIR)/usr/lib/python`python -V 2>&1 | cut -f 2 -d ' ' | cut -f 1-2 -d .`/site-packages/

uninstall:
	rm -f $(DESTDIR)/usr/bin/mad
	rm -f $(DESTDIR)/usr/share/man/man1/mad.1
	rm -f $(DESTDIR)/usr/include/ctss.h
	rm -f $(DESTDIR)/usr/lib/libctss.a
	(cd $(DESTDIR)/usr/lib/python`python -V 2>&1 | cut -f 2 -d ' ' | cut -f 1-2 -d .`/site-packages/; rm -f $(PYTHONPKGS))

libctss/libctss.a:
	(cd libctss; make)

SOURCES = README COPYING NEWS control Makefile ply mad mad.adoc libctss doc/* madhouse/*

mad-$(VERS).tar.gz: $(SOURCES) mad.1
	@find $(SOURCES) mad.1 -print | sed s:^:mad-$(VERS)/: >MANIFEST
	@(cd ..; ln -s mad mad-$(VERS))
	@(cd ..; tar -czf mad/mad-$(VERS).tar.gz `cat mad/MANIFEST`)
	@(cd ..; rm mad-$(VERS))

dist: mad-$(VERS).tar.gz

check:
	@$(MAKE) -C madhouse --quiet check BINDIR=$(realpath $(CURDIR))

release: clean mad-$(VERS).tar.gz mad.html
	shipper version=$(VERS) | sh -e -x

refresh: clean mad.html
	shipper -N -w version=$(VERS) | sh -e -x
