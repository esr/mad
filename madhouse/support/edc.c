#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <ctss.h>

static CtssFile_t infile;
static CtssFile_t outfile;
static int firstread = TRUE;
static int firstwrite = TRUE;
static int wrcnt, rdcnt;

int64_t assign (int64_t name1, int64_t name2)
{
   if (firstwrite) {
      memset (&outfile, 0, sizeof(outfile));
      firstwrite = FALSE;
   }
   GetFileStr (outfile.name, name1, name2);
#ifdef DEBUG
   fprintf (stderr, "assign: name = %s\n", outfile.name);
#endif
   outfile.inuse = TRUE;
   outfile.mode = 'r';

   outfile.fd = fopen (outfile.name, "w");
   wrcnt = 0;
}

int64_t wrword (int64_t *parg)
{
   int64_t x, arg;
   int i;

   arg = *parg;
#ifdef DEBUG
   fprintf (stderr, "wrword: fd = %p, word = %llX: ", outfile.fd, arg);
#endif
   for (i = 0; i < 6; i++) {
      x = (arg & 0xFF0000000000) >> 40;
      fputc (x, outfile.fd);
#ifdef DEBUG
      fputc (x, stderr);
#endif
      arg <<= 8;
   }
   wrcnt++;
   if (wrcnt >= 14) {
      fputc ('\n', outfile.fd);
      wrcnt = 0;
   }
#ifdef DEBUG
   fputc ('\n', stderr);
#endif
}

int64_t file (void)
{
#ifdef DEBUG
   fprintf (stderr, "file: fd = %p\n", outfile.fd);
#endif
   if (outfile.fd)
      fclose (outfile.fd);
   firstwrite = TRUE;
   wrcnt = 0;
}

int64_t seek (int64_t name1, int64_t name2, int64_t *loc)
{
   *loc = 0;
   if (firstread) {
      memset (&infile, 0, sizeof(infile));
      firstread = FALSE;
   }
   GetFileStr (infile.name, name1, name2);
#ifdef DEBUG
   fprintf (stderr, "seek: name = %s\n", infile.name);
#endif
   infile.inuse = TRUE;
   infile.mode = 'r';

   infile.fd = fopen (infile.name, "r");
   rdcnt = 0;
   if (!infile.fd) *loc = 1;
}

int64_t rdword (int64_t *loc)
{
   int64_t x, r;
   int i;
   static int fill = FALSE;

#ifdef DEBUG
   fprintf (stderr, "rdword: fd = %p, word = ", infile.fd);
#endif
   *loc = 0;
   if (fill) {
      rdcnt++;
      if (rdcnt >= 14) {
         fill = FALSE;
	 rdcnt = 0;
      }
      r = 0x202020202020;
#ifdef DEBUG
      fprintf (stderr, "      , r = %llX\n", r);
#endif
      return r;
   }

   r = 0;
   for (i = 0; i < 6; i++) {
      x = fgetc(infile.fd);
      if (x == EOF) {
	 *loc = 1;
	 break;
      }
      if (x == '\n') {
	 if (rdcnt == 0) {
	    i--;
	    continue;
	 }
	 fill = TRUE;
	 break;
      }
#ifdef DEBUG
      fputc (x, stderr);
#endif
      if (islower(x)) x = toupper(x);
      r = r | (x << (40-(i*8)));
   }
   if (fill) {
      x = ' ';
      for (; i < 6; i++) {
	 r = r | (x << (40-(i*8)));
#ifdef DEBUG
	 fputc (x, stderr);
#endif
      }
   }
   rdcnt++;
   if (rdcnt >= 14) {
      fill = FALSE;
      rdcnt = 0;
   }
#ifdef DEBUG
   fprintf (stderr, ", r = %llX\n", r);
#endif
   r &= 0xFFFFFFFFFFFF;
   return r;
}

int64_t endrd (void)
{
#ifdef DEBUG
   fprintf (stderr, "endrd: fd = %p\n", infile.fd);
#endif
   if (infile.fd)
      fclose (infile.fd);
   firstread = TRUE;
   rdcnt = 0;
}

int64_t clrnam (int64_t name1, int64_t name2, int64_t *cant, int64_t sw)
{
   char name[64];
   struct stat filstat;

   GetFileStr (name, name1, name2);
#ifdef DEBUG
   fprintf (stderr, "clrnam: name = %s\n", name);
#endif
   if (stat (name, &filstat) == 0) {
      char ans;

      ans = 'Y';
      if (sw) {
	 
	 printf ("Old File: %s, Do wish to delete: ", name);
	 ans = getchar();
      }
      if (ans == 'Y' || ans == 'y') {
         unlink (name);
      }
   }
}

int64_t prword (int64_t arg)
{
   int64_t x;
   int i;

   for (i = 0; i < 6; i++) {
      x = (arg & 0xFF0000000000) >> 40;
      if (!x)
         break;
      fputc (x, stdout);
      arg <<= 8;
   }
}

int64_t prbuff (void)
{
   fflush (stdout);
}

int64_t tyword (int64_t loc)
{
   int64_t x, r;
   int i;

   r = 0;
   for (i = 0; i < 6; i++) {
      x = fgetc(stdin);
      if (x == EOF) break;
      if (islower(x)) x = toupper(x);
      r = r | (x << (40-(i*8)));
      if (x == '\n') break;
   }
   r &= 0xFFFFFFFFFFFF;
   return r;
}

int64_t tyline (void)
{
}

int64_t setful (void)
{
}

int64_t derbc (int64_t arg)
{
   int64_t r;
   char tmp[32];
   sprintf (tmp, "%06lld", arg & 0777777);
   r = PackStr (tmp) & 0x0F0F0F0F0F0F;
#ifdef DEBUG
   fprintf (stderr, "derbc: tmp = %s, r = %llx\n", tmp, r);
#endif
   return r;
}
